# Peter Kopias 
## Senior Software Architect / Robotic Engineer

20+ years in Software Development and Architecture 

## Responsibilities
- Design and build for reliability and maintenance
- Create and review high level designs and implementations
- Plan data streams and development/operations processes
- Compare alternative solutions and make suggestions to business stakeholders
- Routinely handles teams of 10

## Skills
- Backend systems: Python, C, Php, C++, C#, Bash, Java
- Docker, Kubernetes, Networking & SDN, RDBMS, HA, Graceful degradation, Ansible, Mysql, Postgresql, ROS, RabbitMQ, Git, RestApi, Semantic Versioning, Stream processing, Protocols, Middleware & Serialization, Embedded systems, Continuous Delivery
- Prefers Agile environment, Continuous integration and automatic delivery, Infrastructure as code, Devops, Containerization
- Soft skills: Leadership, Public speaking, Empathy
- Interested to work with Go, Dart, Rust and Flutter
- Appetite for mechanical robot designs

## Education
- B.Sc. in Computer Engineering (spec: ERP systems)
- Certified Kubernetes Administrator
- English B2

## Experience
#### 1998- SWI Kft - Search Engine Startup
- Founder / CTO for 6 years
- Built the software stack behind the services
- Linux, C, Php, Apache, Mysql

#### 2004- Dunakanyar Holding & 3C Telecom - Internet Service Provider
- Team Lead / Software Architect
- Introduced Version Control, Agile, CI/CD, Infrastructure as code
- C, Php, Mysql, Asterisk, Vagrant, Ansible, Virtualization, Rabbitmq, Subversion, Git

#### 2017- IContest - Software Development Services
- Consultant / Senior Architect
- Multiple projects, On premise clouds, SDLC pipelines, Quality gates, Data analytics dashboards
- Clients include: Energy, Finance, State, O&G, Telecom
- Kubernetes, Openshift, Helm, nginx, Python, Git, Gitlab, Elasticsearch, Kibana

#### 2020- Datastart - Robotics Prototype Development
 - Owner / CEO
 - Custom mobile robotic prototypes
 - ROS, Python, C++, Docker, Gitlab etc.

## Achievements 
 - Two times national champion in competitive programming
 - Innovation Award (FTP Search Engine)
 - RoboCup WorldCup Design Award and Best Outdoor Carrybot Award
 - World Robot Summit Tokyo
 - Enrich Robotics Hackathon in active environment: https://www.youtube.com/watch?v=tUozGwsI_Dk
 - IAEA Robotics Challenge https://www.iaea.org/newscenter/news/robotics-challenge-winning-design-helps-speed-up-spent-fuel-verification

## Contact

 - https://www.linkedin.com/in/peterkopias/
 - peter.kopias@rainboat.io
 - Budapest, Hungary
